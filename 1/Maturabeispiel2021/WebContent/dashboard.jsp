<%@page import="at.htlklu.fsst.sql.RunDao"%>
<%@page import="at.htlklu.fsst.pojo.Run"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Distance Running Dashboard</title>
<style type="text/css">
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

th {
	background-color: #0099ff;
	color: #ffffff;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #f2f2f2;
}
</style>
</head>
<body>
	<h1>Distance Running Dashboard</h1>

	<%
	String likingIdS = request.getParameter("like");

	if (likingIdS != null) {
		try {
			int likingId = Integer.parseInt(likingIdS);
			RunDao.likeRun(likingId);
	%>
	<h2>
		Liking Run with ID
		<%=likingId%></h2>
	<%
	} catch (NumberFormatException e) {
	e.printStackTrace();
	}
	}
	%>

	<table>
		<tr>
			<th>Lauf ID</th>
			<th>Läufername</th>
			<th>Laufdauer</th>
			<th>km-Anzahl</th>
			<th>Laufbeschreibung</th>
			<th>Likes</th>
		</tr>
		<%
		List<Run> runs = RunDao.getAllRuns();

		for (Run run : runs) {
		%>
		<tr>
			<td><%=run.getId()%></td>
			<td><%=run.getName()%></td>
			<td><%=run.getTime()%></td>
			<td><%=String.format("%.2f", run.getDistance())%></td>
			<td><%=run.getDescriptionAsString()%></td>
			<td><%=run.getLikes()%> <a
				href="dashboard.jsp?like=<%=run.getId()%>">👍</a></td>
		</tr>
		<%
		}
		%>
	</table>
</body>
</html>