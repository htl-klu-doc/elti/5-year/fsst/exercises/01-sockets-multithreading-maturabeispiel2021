package at.htlklu.fsst.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import at.htlklu.fsst.helper.DbConnection;
import at.htlklu.fsst.pojo.Run;

public class RunDao {
	public static void main(String[] args) {
//		addRun(new Run("Franz", 4.13, "Blaues T-Shirt", 55, Timestamp.valueOf("2021-09-20 16:00:00"),
//		Timestamp.valueOf("2021-09-20 16:32:39")));
		//int id = startRun(new Run("Sun", Timestamp.from(Instant.now())));
		// endRun(new Run(7, 6.3, null, Timestamp.from(Instant.now())));
		// likeRun(7);
		//System.out.println(id);
		List<Run> runs = getAllRuns();

		for (Run run : runs) {
			System.out.println(run);
		}
	}

	private static Run mapRow(ResultSet rset) throws SQLException {
		return new Run(rset.getInt(1), rset.getString(2), rset.getDouble(3), rset.getString(4), rset.getInt(5),
				rset.getTimestamp(6), rset.getTimestamp(7));
	}

	public static List<Run> getAllRuns() {
		ArrayList<Run> runs = new ArrayList<>();
		String sql = "SELECT * FROM run ORDER BY likes DESC, name";

		try (PreparedStatement stmt = DbConnection.getConnection().prepareStatement(sql)) {
			ResultSet rset = stmt.executeQuery();

			while (rset.next())
				runs.add(mapRow(rset));

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return runs;
	}

	public static int addRun(Run run) {
		String sql = "INSERT INTO run (name, distance, description, likes, startDate, endDate) VALUES (?, ?, ?, ?, ?, ?)";

		try (PreparedStatement pstmt = DbConnection.getConnection().prepareStatement(sql,
				Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, run.getName());
			pstmt.setDouble(2, run.getDistance());
			pstmt.setString(3, run.getDescription());
			pstmt.setInt(4, run.getLikes());
			pstmt.setTimestamp(5, run.getStartDate());
			pstmt.setTimestamp(6, run.getEndDate());

			pstmt.execute();
			ResultSet rset = pstmt.getGeneratedKeys();

			if (rset.next())
				return rset.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static int startRun(Run run) {
		String sql = "INSERT INTO run (name, startDate) VALUES (?, ?)";

		try (PreparedStatement pstmt = DbConnection.getConnection().prepareStatement(sql,
				Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, run.getName());
			pstmt.setTimestamp(2, run.getStartDate());

			pstmt.execute();
			ResultSet rset = pstmt.getGeneratedKeys();

			if (rset.next())
				return rset.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static boolean endRun(Run run) {
		String sql = "UPDATE run SET distance = ?, description = ?, endDate = ? WHERE (id = ?)";

		try (PreparedStatement pstmt = DbConnection.getConnection().prepareStatement(sql)) {
			pstmt.setDouble(1, run.getDistance());
			pstmt.setString(2, run.getDescription());
			pstmt.setTimestamp(3, run.getEndDate());
			pstmt.setInt(4, run.getId());

			return pstmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean likeRun(int idRun) {
		String sql = "UPDATE run SET likes = likes+1 WHERE (id = ?)";

		try (PreparedStatement pstmt = DbConnection.getConnection().prepareStatement(sql)) {
			pstmt.setInt(1, idRun);

			return pstmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
