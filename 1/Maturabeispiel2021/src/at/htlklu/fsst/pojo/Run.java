package at.htlklu.fsst.pojo;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Run {
	private int id;
	private String name;
	private double distance;
	private String description;
	private int likes;
	private Timestamp startDate;
	private Timestamp endDate;

	public Run(int id, String name, double distance, String description, int likes, Timestamp startDate,
			Timestamp endDate) {
		this(name, distance, description, likes, startDate, endDate);
		this.setId(id);
	}

	public Run(String name, double distance, String description, int likes, Timestamp startDate, Timestamp endDate) {
		this.setName(name);
		this.setDistance(distance);
		this.setDescription(description);
		this.setLikes(likes);
		this.setStartDate(startDate);
		this.setEndDate(endDate);
	}

	public Run(String name, Timestamp startDate) {
		this.setName(name);
		this.setStartDate(startDate);
	}

	public Run(int id, double distance, String description, Timestamp endDate) {
		this.setId(id);
		this.setDistance(distance);
		this.setDescription(description);
		this.setEndDate(endDate);
	}

	@Override
	public String toString() {
		return String.format("[%02d] %35s | %5.2f km | %45s | %3d | %21s | %21s | %s", this.getId(), this.getName(),
				this.getDistance(), this.getDescriptionAsString(), this.getLikes(), this.getStartDateAsString(),
				this.getEndDateAsString(), this.getTime());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getDescription() {
		return description;
	}

	public String getDescriptionAsString() {
		if (description == null)
			return "No description provided!";
		else
			return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public String getStartDateAsString() {
		if (this.startDate == null)
			return "Not started yet";
		else
			return this.startDate.toString();
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public String getEndDateAsString() {
		if (this.endDate == null)
			return "Not finished yet";
		else
			return this.endDate.toString();
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getTime() {
		if (this.getStartDate() == null || this.getEndDate() == null)
			return "-";

		LocalDateTime startDateLDT = this.getStartDate().toLocalDateTime();
		LocalDateTime endDateLDT = this.getEndDate().toLocalDateTime();

		//long millis = ChronoUnit.MILLIS.between(startDateLDT, endDateLDT) % 1000;
		long seconds = ChronoUnit.SECONDS.between(startDateLDT, endDateLDT) % 60;
		long minutes = ChronoUnit.MINUTES.between(startDateLDT, endDateLDT) % 60;
		long hours = ChronoUnit.HOURS.between(startDateLDT, endDateLDT) % 24;

		StringBuilder stb = new StringBuilder();

		// if (hours != 0)
		stb.append(String.format(" %02d h", hours));
		stb.append(String.format(" %02d min", minutes));

		// if (seconds != 0)
		stb.append(String.format(" %02d s", seconds));
		// if (millis != 0)
		//stb.append(String.format(" %03d ms", millis));

		return stb.toString();
	}
}
