package at.htlklu.fsst.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SocketServer {
	public static final int PORT = 5710;
	public static int connections = 0;

	public static void main(String[] args) {
		ArrayList<ServerThread> threads = new ArrayList<>();
		Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
				() -> System.out.printf("Currently handling %03d connections%n", connections), 0, 1, TimeUnit.SECONDS);

		try (ServerSocket serverSocket = new ServerSocket(PORT)) {
			while (true) {
				Socket client = serverSocket.accept();
				ServerThread st = new ServerThread(client);
				threads.add(st);
				st.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
