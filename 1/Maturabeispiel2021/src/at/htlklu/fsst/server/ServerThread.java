package at.htlklu.fsst.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.time.Instant;

import at.htlklu.fsst.pojo.Run;
import at.htlklu.fsst.sql.RunDao;

public class ServerThread extends Thread {

	public static final String VERSION = "v1.0";
	private Socket client;

	public ServerThread(Socket client) {
		this.client = client;
		SocketServer.connections++;
	}

	@Override
	public void run() {
		try {
			// communtication
			InputStream inputStream = client.getInputStream();
			OutputStream outputStream = client.getOutputStream();
			BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
			PrintWriter output = new PrintWriter(outputStream, true);

			output.println(String.format("Distance Running Server %s", VERSION));

			communication(input, output);

			output.close();
			input.close();
			outputStream.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SocketServer.connections--;
	}

	private static void communication(BufferedReader input, PrintWriter output) {
		boolean keepRunning = true;

		while (keepRunning) {
			String answer;
			try {
				answer = input.readLine();
				keepRunning = handleInput(answer, output);
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
				keepRunning = false;
			}
		}
	}

	private static boolean handleInput(String answer, PrintWriter output) {
		if (answer == null) {
			output.print("Closing connection");
			output.close();
			return false;
		}

		String[] tokens = answer.split(" ");

		switch (tokens[0]) {
		case "STARTRUN": {
			try {
				String name = tokens[1];
				int id = RunDao.addRun(new Run(name, Timestamp.from(Instant.now())));

				if (id == -1)
					output.print("UNKNOWN RUN");
				else
					output.print(String.format("Hallo %s, Ihre Lauf-ID ist %03d", name, id));

			} catch (Exception e) {
				output.print("Invalid name!");
			}
			break;
		}
		case "STOPRUN": {
			try {
				String data = answer.substring(8);
				String[] values = data.split("\\$");
				int id = Integer.parseInt(values[0]);
				double distance = Double.parseDouble(values[1]);
				String description = values[2];

				RunDao.endRun(new Run(id, distance, description, Timestamp.from(Instant.now())));
				output.print(String.format("Ok, Lauf mit der ID %03d beendet", id));
			} catch (Exception e) {
				output.print("Invalid data!");
			}
			break;
		}
		case "EXIT": {
			output.print("Closing connection");
			output.close();
			return false;
		}
		default:
			output.print("UNKNOWN COMMAND");
			// throw new IllegalArgumentException("Unexpected value: " + tokens[0]);
			// return false;
		}

		output.println();
		return true;
	}
}
